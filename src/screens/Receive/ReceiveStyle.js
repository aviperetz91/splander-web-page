const styles = {
    badge: {
        fontSize: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        position: 'absolute',
        right: -27,
        top: 31
    },
    badge2: {
        fontSize: 20,
        backgroundColor: 'white',
        borderRadius: 50,
        position: 'absolute',
        top: -16,
        right: 21,
    }
}

export default styles;